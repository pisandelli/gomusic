/*jslint browser: true*/
/*global module*/
/*jscs:disable requireCamelCaseOrUpperCaseIdentifiers*/
/*jshint -W106 */

(function () {
    'use strict';

    module.exports = function () {

        //paths for application
        var app_root = 'src',
            build_path = 'dist',

            config = {

                //define
                root: app_root,
                concat_css_file: 'main.css',

                //sass workflow
                all_sass: app_root + '/styles/sass/**/*.{scss,sass}',
                main_sass: app_root + '/styles/sass/main.scss',
                init_js: app_root + '/scripts/init.js',

                //other sources files
                all_js: app_root + '/scripts/**/*.js',
                all_templates: app_root + '/templates/*.pug',
                all_pug: app_root + '/templates/**/*.pug',
                all_css: app_root + '/styles/*.css',
                all_img: app_root + '/images/**/*.{jpg,png,gif,svg}',
                all_fonts: app_root + '/fonts/**/*.{eot,svg,ttf,woff}',
                all_videos: app_root + '/videos/**/*.{mp4,ogv,ogg,webm}',
                all_html: app_root + '/*.html',
                pug_config: app_root + '/templates/pug.config.json',

                //destinations
                css_folder: app_root + '/styles',
                scripts_folder: app_root + '/scripts',
                maps_folder: '.maps',
                sass_folder: app_root + '/styles/sass',
                images_folder: app_root + '/images',

                //Build
                build: {

                    html: build_path,
                    img: build_path + '/images',
                    fonts: build_path + '/fonts',
                    videos: build_path + '/videos'
                }
            };

        return config;
    };
}());
