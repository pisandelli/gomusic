/*jslint browser: true*/
/*global module*/
/*jscs:disable requireCamelCaseOrUpperCaseIdentifiers*/
/*jshint -W106 */

(function () {
    'use strict';

    module.exports = function () {

        var app_root = 'src',

            base_sass = app_root + '/styles/sass/base/*.{scss,sass}',
            layouts_sass = app_root + '/styles/sass/layouts/*.{scss,sass}',
            modules_sass = app_root + '/styles/sass/modules/*.{scss,sass}',

            config = {
                'base': {
                    'path': base_sass,
                    'starttag': '//base',
                    'endtag': '//endbase',
                    'dependencies': []
                },
                'layouts': {
                    'path': layouts_sass,
                    'starttag': '//layouts',
                    'endtag': '//endlayouts',
                    'dependencies': ['base']
                },
                'modules': {
                    'path': modules_sass,
                    'starttag': '//modules',
                    'endtag': '//endmodules',
                    'dependencies': ['layouts']
                }
            };
        return config;
    };
}());
