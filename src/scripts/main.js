/*jslint browser: true*/
/*global document*/

//TOP-BAR
(function () {
    'use strict';

    //TOP BAR ACTIONS (MENU SHOW, BURGER ICON ANIMATION, ETC...)
    var topMenuBar = document.getElementById('topbar'),
        triggers = topMenuBar.querySelectorAll('#burger, .item, #brand'),
        i;

    function showMenu(id, event) {
        //event.preventDefault();
        if (id === 'brand') {
            document.getElementById('main_nav').classList.remove('show');
            document.getElementById('burger').classList.remove('open');
        } else {
            document.getElementById('main_nav').classList.toggle('show');
            document.getElementById('burger').classList.toggle('open');
        }
    }

    for (i = 0; i < triggers.length; i += 1) {
        triggers[i].addEventListener('click', showMenu.bind(null, triggers[i].id), false);
    }
}());
