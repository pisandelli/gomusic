/*jslint browser: true*/
/*global require, log, clean, console, filepath*/
/*jscs:disable requireCamelCaseOrUpperCaseIdentifiers*/
/*jshint -W106, -W089*/

(function () {

    'use strict';

    var gulp = require('gulp'),
        config = require('./gulp.config')(),
        sass_config = require('./sass.config')(),
        del = require('del'),
        path = require('path'),
        //wiredep = require('wiredep'),
        serveStatic = require('serve-static'),
        runSequence = require('run-sequence'),
        //mainBowerFiles = require('main-bower-files'),
        fse = require('fs-extra'),
        gif = require('gulp-if'),
        $ = require('gulp-load-plugins')({lazy: true}),
        defaultTasks = [];


    /////////////////////
    /// DEV TASKS
    /////////////////////

    //Compiling PUG files
    gulp.task('compile_pug', function () {
        log('Compiling Pug Templates');

        return gulp
            .src(config.all_templates)
            .pipe($.plumber({
                handleError: function (err) {
                    log(err);
                    this.emit('end');
                }
            }))
            .pipe($.print())
            .pipe($.data(function (file) {
                return require(path.resolve(config.pug_config));
            }))
            .pipe($.pug({
                pretty: true
            }))
            .pipe(gulp.dest(config.root));
    });

    //compile pug and perform wiredep
    gulp.task('pug_wiredep', function (callback) {
        log('Compiling Pug Templates and injecting dependencies');
        runSequence('compile_pug', 'inject_js',/*'wiredep',*/ callback);
    });

    //SASS WORKFLOW

    //Task Template
    function createTasks(key) {
        gulp.task(key, sass_config[key].dependencies, function () {
            log('Injecting ' + key + ' files');
            var source = gulp.src(sass_config[key].path, {read: false});
            return gulp
                .src(config.main_sass)
                .pipe($.plumber({
                    handleError: function (err) {
                        log(err);
                        this.emit('end');
                    }
                }))
                .pipe($.print())
                .pipe($.inject(source, {
                    ignorePath: config.sass_folder,
                    addRootSlash: false,
                    starttag: sass_config[key].starttag,
                    endtag: sass_config[key].endtag,
                    transform: function (filepath) {
                        return '@import "' + filepath + '";';
                    }
                }))
                .pipe(gulp.dest(config.sass_folder));
        });
    }

    //start fill the task list for Sass
    (function () {
        var key;
        //loop to create tasks dynamically from sass.config.js
        for (key in sass_config) {
            if (sass_config.hasOwnProperty(key)) {
                createTasks(key);
                defaultTasks.push(key);
            }
        }
    }());

    //Compiling SASS files
    gulp.task('compile_sass', [defaultTasks[defaultTasks.length - 1]], function () {
        log('Compiling Sass');

        return gulp
            .src(config.main_sass)
            .pipe($.plumber({
                handleError: function (err) {
                    log(err);
                    this.emit('end');
                }
            }))
            .pipe($.print())
            .pipe($.sourcemaps.init())
            .pipe($.sass({
                outputStyle: 'expanded'
            }))
            .pipe($.autoprefixer({browsers: ['last 2 version', '> 5%', 'ie 10', 'ie 11']}))
            .pipe($.concat(config.concat_css_file))
            .pipe($.sourcemaps.write(config.maps_folder))
            .pipe(gulp.dest(config.css_folder));
    });

    //Check JS
    gulp.task('check_js', function () {
        log('Checking JS files');

        return gulp
            .src(config.all_js)
            .pipe($.plumber({
                handleError: function (err) {
                    log(err);
                    this.emit('end');
                }
            }))
            .pipe($.print())
            .pipe($.jscs())
            .pipe($.jshint())
            .pipe($.jshint.reporter('jshint-stylish', {verbose: true}))
            .pipe($.jshint.reporter('fail'));
    });

    //Inject new JS files
    gulp.task('inject_js', ['check_js'], function () {
        log('Injecting Custom JS');
        var sources = gulp.src(config.all_js, {read: false});

        return gulp
            .src(config.all_html)
            .pipe($.print())
            .pipe($.inject(sources, {
                ignorePath: config.root,
                addRootSlash: false
            }))
            .pipe(gulp.dest(config.root));
    });

    //Inject Bower Components
    // gulp.task('wiredep', function () {
    //     log('Injecting Bower Components');
    //
    //     wiredep = require('wiredep').stream;
    //
    //     return gulp
    //         .src(config.all_html)
    //         .pipe($.plumber({
    //             handleError: function (err) {
    //                 log(err);
    //                 this.emit('end');
    //             }
    //         }))
    //         .pipe($.print())
    //         .pipe(wiredep())
    //         .pipe(gulp.dest(config.root));
    // });


    /////////////////////
    /// BUILD TASKS
    /////////////////////

    //compress images
    gulp.task('compress_images', function () {
        log('Compressing Images to ' + config.build.img);

        return gulp
            .src(config.all_img)
            .pipe($.print())
            .pipe($.imagemin({
                optimizationLevel: 3,
                progressive: true,
                interlaced: true
            }))
            .pipe(gulp.dest(config.build.img));
    });

    //copy font files
    // gulp.task('copy_fonts', ['copy_vendor_fonts'], function () {
    //     log('Copying fonts to ' + config.build.fonts);
    //
    //     return gulp
    //         .src(config.all_fonts)
    //         .pipe(gulp.dest(config.build.fonts));
    //
    // });
    //
    // gulp.task('copy_vendor_fonts', function () {
    //     log('Copying vendor fonts');
    //
    //     return gulp
    //         .src(mainBowerFiles())
    //         .pipe($.filter('**/*.{eot,svg,ttf,woff}'))
    //         .pipe(gulp.dest(config.build.fonts));
    // });

    //copy video files
    // gulp.task('copy_videos', function () {
    //     log('Copying videos to ' + config.build.videos);
    //
    //     return gulp
    //         .src(config.all_videos)
    //         .pipe(gulp.dest(config.build.videos));
    //
    // });

    //Concatenate CSS & JS in HTML files
    gulp.task('concat_html', function () {
        log('Concatenating Scripts and CSS files in BUILD tags');

        return gulp
            .src(config.all_html)
            .pipe($.plumber({
                handleError: function (err) {
                    log(err);
                    this.emit('end');
                }
            }))
            .pipe($.useref())
            // .pipe(gif('*.js', $.uglify()))
            // .pipe(gif('*.css', $.csso()))
            .pipe(gulp.dest(config.build.html));
    });

    //Concatenate CSS & JS in HTML files
    // gulp.task('uglify_html', function () {
    //     log('Compressing HTML files');
    //
    //     return gulp
    //         .src(config.all_html)
    //         .pipe($.plumber({
    //             handleError: function (err) {
    //                 log(err);
    //                 this.emit('end');
    //             }
    //         }))
    //         .pipe($.minifyHtml({
    //             empty: true
    //         }))
    //         .pipe(gulp.dest(config.build.html));
    // });

    //Clean dist folder
    gulp.task('clean_dist', function () {
        return clean(config.build.html);
    });

    //BUILD
    gulp.task('build', ['clean_dist'], function (callback) {
        log('Starting BUILD sequence');
        runSequence(
            'compile_pug',
            'compile_sass',
            'inject_js',
            //'wiredep',
            'concat_html',
            'compress_images',
            //'copy_fonts',
            //'uglify_html',
            callback
        );
    });

    /////////////////
    /// SERVER
    /////////////////

    gulp.task('connect', function () {
        var connect = require('connect'),
            app = connect()
                .use(require('connect-livereload')({ port: 35729 }))
                .use(serveStatic(config.root));

        require('http').createServer(app)
            .listen(9000)
            .on('listening', function () {
                console.log('Web server is running on http://localhost:9000');
            });
    });

    gulp.task('serve', ['connect'], function () {
        require('opn')('http://localhost:9000');
    });

    /////////////////
    /// WATCH FILES
    /////////////////

    gulp.task('watch', ['connect', 'serve'], function () {
        log('Files are now under surveillance!');

        var server = $.livereload.listen();

        //watch for changes and reload
        gulp.watch([
            config.all_html,
            config.all_css,
            config.all_js])
            .on('change', $.livereload.changed);

        gulp.watch(config.all_pug, ['pug_wiredep']);
        gulp.watch([
            config.sass_folder + '/**',
            config.all_sass,
            '!' + config.main_sass],
            ['compile_sass']);
        gulp.watch(config.all_js, ['inject_js']);
        //gulp.watch('bower.json', ['wiredep']);
    });

    //START CHAIN
    gulp.task('start', function (callback) {
        log('Start Workflow');
        runSequence('compile_pug', 'compile_sass', 'inject_js',/*  'wiredep',*/ 'watch', callback);
    });

    //SETUP
    gulp.task('install', function () {
        log('Environment\'s Setup');
        var key,
            wrapString = '';

        for (key in sass_config) {
            if (sass_config.hasOwnProperty(key)) {
                wrapString += sass_config[key].starttag + '\n' + sass_config[key].endtag + '\n';
                fse.mkdirsSync(config.sass_folder + '/' + key);
            }
        }
        fse.createOutputStream(config.main_sass).write(wrapString);
        fse.createOutputStream(config.init_js);
        fse.mkdirsSync(config.images_folder);
    });

    gulp.task('setup', ['install'], function () {
        gulp.start('start');
    });

    /////////////////////
    /// OTHER FUNCTIONS
    /////////////////////


    function clean(path, done) {
        log('Cleaning: ' + $.util.colors.yellow(path));
        del(path, done);
    }

    function log(msg) {
        var item;
        if (typeof msg === 'object') {
            for (item in msg) {
                if (msg.hasOwnProperty(item)) {
                    $.util.log($.util.colors.white(msg[item]));
                }
            }
        } else {
            $.util.log($.util.colors.white(msg));
        }
    }
}());
